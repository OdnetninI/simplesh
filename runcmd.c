#include "common.h"

// Ejecuta un `cmd`.
void run_cmd(struct cmd *cmd) {
  int p[2];
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;
  pid_t pid = 0;
  pid_t pid2 = 0;

  if(cmd == 0)
    exit(0);

  if (getCMDName(cmd) == NULL) return;

  switch(cmd->type) {
    default:
    panic("run_cmd");

    // Ejecución de una única orden.
    case EXEC:
      ecmd = (struct execcmd*)cmd;
      if (isSpecialCMD(getCMDName(cmd)))
        exec_internal(isBuiltIn(getCMDName(cmd)), ecmd->argv);
      else {
        if( (pid = fork1()) == 0) {
          if (ecmd->argv[0] == 0)
            exit(0);
          int n = isBuiltIn(getCMDName(cmd));
          if (!n) {
            execvp(ecmd->argv[0], ecmd->argv);
            // Si se llega aquí algo falló
            fprintf(stderr, "exec %s failed\n", ecmd->argv[0]);
            exit (1);
          }
          else {
            exec_internal(n, ecmd->argv);
            exit (0);
          }
        }
        waitAndKillChild(pid);
      }
    break;

    case REDIR:
      rcmd = (struct redircmd*)cmd;
      if( (pid = fork1()) == 0) {
        close(rcmd->fd);
        int od = open(rcmd->file, rcmd->mode, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);
        if (od < 0) {
          fprintf(stderr, "open %s failed\n", rcmd->file);
          exit(1);
        }
        run_cmd(rcmd->cmd);
        exit(0);
      }
      waitAndKillChild(pid);

      if (isSpecialCMD(getCMDName(cmd)))
        run_cmd(rcmd->cmd);
      break;

    case LIST:
      lcmd = (struct listcmd*)cmd;
      run_cmd(lcmd->left);
      run_cmd(lcmd->right);
      break;

    case PIPE:
      pcmd = (struct pipecmd*)cmd;
      if (pipe(p) < 0)
        panic("pipe");

      // Ejecución del hijo de la izquierda
      if((pid = fork1()) == 0) {
        close(1);
        dup(p[1]);
        close(p[0]);
        close(p[1]);
        run_cmd(pcmd->left);
        exit(0);
      }

      // Ejecución del hijo de la derecha
      if((pid2 = fork1()) == 0) {
        close(0);
        dup(p[0]);
        close(p[0]);
        close(p[1]);
        run_cmd(pcmd->right);
        exit(0);
      }
      close(p[0]);
      close(p[1]);

      // Esperar a ambos hijos
      waitAndKillChild(pid);
      waitAndKillChild(pid2);
      break;

    case BACK:
      bcmd = (struct backcmd*)cmd;
      if(fork1() == 0)
        run_cmd(bcmd->cmd);
      break;
  }

  // Salida normal, código 0.
  //exit(0);
}
