#include "common.h"

static unsigned long long int N = 0;
static unsigned long long int timeout = 5;

void signalHandler (int sig) {
  switch (sig) {
    case SIGCHLD: N++; break;
    case SIGUSR1: if (timeout < ULLONG_MAX - 5) timeout += 5; break;
    case SIGUSR2: if (timeout > 5) timeout -= 5; break;
  }
}

void waitAndKillChild(pid_t pid) {
  sigset_t signals;
  sigemptyset (& signals);
  sigaddset (& signals , SIGCHLD);
  struct timespec _timeout;
  _timeout.tv_sec = timeout;
  _timeout.tv_nsec = 0;
  if (sigtimedwait(&signals, NULL, &_timeout) == -1) {
    sigset_t  blocked_signals;
    sigemptyset (& blocked_signals);
    sigaddset (& blocked_signals , SIGCHLD);
    if (sigprocmask(SIG_UNBLOCK , &blocked_signals , NULL) ==  -1) {
      perror("sigprocmask");
      exit(EXIT_FAILURE);
    }
    kill(pid, 9);
    waitpid(pid, NULL, 0);
    if (sigprocmask(SIG_BLOCK , &blocked_signals , NULL) ==  -1) {
      perror("sigprocmask");
      exit(EXIT_FAILURE);
    }
    fprintf(stderr, "simplesh: [%lld] Matado hijo con PID %d\n", N, pid);
  }
  else waitpid(pid, NULL, 0);
}

void createSignals () {
  struct sigaction sa;
  sa.sa_handler = signalHandler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;

  sigset_t  blocked_signals;
  sigemptyset (& blocked_signals);
  sigaddset (& blocked_signals , SIGINT);
  sigaddset (& blocked_signals , SIGCHLD);
  if (sigprocmask(SIG_BLOCK , &blocked_signals , NULL) ==  -1) {
    perror("sigprocmask");
    exit(EXIT_FAILURE);
  }

  if (sigaction(SIGCHLD , &sa, NULL) ==  -1) {
    perror("sigaction");
    exit(EXIT_FAILURE);
  }
  if (sigaction(SIGUSR1 , &sa, NULL) ==  -1) {
    perror("sigaction");
    exit(EXIT_FAILURE);
  }
  if (sigaction(SIGUSR2 , &sa, NULL) ==  -1) {
    perror("sigaction");
    exit(EXIT_FAILURE);
  }
}
