#ifndef __INTERNALS_H__
#define __INTERNALS_H__

void exec_exit();

/**
* exec_pwd
* Executes pwd using posix calls
* Writes directory in standar output
* Writes "simplesh: pwd: " in standar error output
*/
void exec_pwd();

/**
* exec_cd
* Executes cd using posix calls
* Change the actual dir to argv[1]
* If not dir specified, chage dir to $HOME
*/
void exec_cd (char* argv[]);

/**
* exec_tee
* Executes tee using posix calls
* write input to output or file/files
*/
void exec_tee (char* argv[]);

void exec_du (char* argv[]);

#endif /* __INTERNALS_H__ */
