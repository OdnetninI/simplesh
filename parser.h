#ifndef __PARSER_H__
#define __PARSER_H__

#include "common.h"

// Obtiene un *token* de la cadena de entrada `ps`, y hace que `q` apunte a
// él (si no es `NULL`).
int gettoken(char **ps, char *end_of_str, char **q, char **eq);

// La función `peek()` recibe un puntero a una cadena, `ps`, y un final de
// cadena, `end_of_str`, y un conjunto de tokens (`toks`). El puntero
// pasado, `ps`, es llevado hasta el primer carácter que no es un espacio y
// posicionado ahí. La función retorna distinto de `NULL` si encuentra el
// conjunto de caracteres pasado en `toks` justo después de los posibles
// espacios.
int peek(char **ps, char *end_of_str, char *toks);

// Función principal que hace el *parsing* de una línea de órdenes dada por
// el usuario. Llama a la función `parse_line()` para obtener la estructura
// `cmd`.
struct cmd* parse_cmd(char *s);

// *Parsing* de una línea. Se comprueba primero si la línea contiene alguna
// tubería. Si no, puede ser un comando en ejecución con posibles
// redirecciones o un bloque. A continuación puede especificarse que se
// ejecuta en segundo plano (con `&`) o simplemente una lista de órdenes
// (con `;`).
struct cmd* parse_line(char **ps, char *end_of_str);

// *Parsing* de una posible tubería con un número de órdenes.
// `parse_exec()` comprobará la orden, y si al volver el siguiente *token*
// es un `'|'`, significa que se puede ir construyendo una tubería.
struct cmd* parse_pipe(char **ps, char *end_of_str);

// Construye los comandos de redirección si encuentra alguno de los
// caracteres de redirección.
struct cmd* parse_redirs(struct cmd *cmd, char **ps, char *end_of_str);

// *Parsing* de un bloque de órdenes delimitadas por paréntesis.
struct cmd* parse_block(char **ps, char *end_of_str);

// Hace en *parsing* de una orden, a no ser que la expresión comience por
// un paréntesis. En ese caso, se inicia un grupo de órdenes para ejecutar
// las órdenes de dentro del paréntesis (llamando a `parse_block()`).
struct cmd* parse_exec(char **ps, char *end_of_str);

#endif /* __PARSER_H__ */
