#ifndef __COMMON_H__
#define __COMMON_H__

#define _XOPEN_SOURCE 500

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <pwd.h>
#include <libgen.h>
#include <getopt.h>
#include <time.h>
#include <ftw.h>
#include <fcntl.h>
#include <signal.h>
#include <limits.h>

// Libreadline
#include <readline/readline.h>
#include <readline/history.h>

// simplesh Includes
#include "const.h"
#include "structs.h"
#include "internals.h"
#include "parser.h"
#include "runcmd.h"
#include "specialCMD.h"
#include "signalHandler.h"

int fork1(void);
void panic(char*);

#endif /* __COMMON_H__ */
