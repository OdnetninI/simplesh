// Shell `simplesh`
#include "common.h"

// Muestra un *prompt* y lee lo que el usuario escribe usando la
// librería readline. Ésta permite almacenar en el historial, utilizar
// las flechas para acceder a las órdenes previas, búsquedas de
// órdenes, etc.
char* getcmd() {
  char *buf;
  int retval = 0;

  // Get actual user
  struct passwd* user = getpwuid(getuid());
  if (user == NULL) {
    perror("getpwuid");
    exit(EXIT_FAILURE);
  }

  // Get Actual Dir
  char directory[MAX_PATH_LENGTH];
  if (!getcwd(directory, MAX_PATH_LENGTH)) {
    perror("getcwd");
    exit(EXIT_FAILURE);
  }

  // Create prompt syntax
  char prompt [MAX_PROMPT_LENGTH*sizeof(char)];
  snprintf(prompt, MAX_PROMPT_LENGTH, "\x1B[0m[\x1B[31m%s\x1B[34m@\x1B[33m%s\x1B[0m]\x1B[32m$\x1B[0m ", user->pw_name, basename(directory));

  // Lee la entrada del usuario
  buf = readline (prompt);

  // Si el usuario ha escrito algo, almacenarlo en la historia.
  if(buf)
      add_history (buf);

  return buf;
}

// Función `main()`.
// ----

int main(void) {

  createSignals();

  char* buf;
  // Bucle de lectura y ejecución de órdenes.
  while (NULL != (buf = getcmd())) {
    run_cmd(parse_cmd(buf));
    free ((void*)buf);
  }

  return 0;
}
