#ifndef __SPECIALCMD_H__
#define __SPECIALCMD_H__

#include "common.h"

#define _CMDS     5
#define _CMD_NOPINTERNAL 0
#define _CMD_EXIT 1
#define _CMD_CD   2
#define _CMD_PWD  3
#define _CMD_TEE  4
#define _CMD_DU   5

void exec_internal (unsigned int number, char* argv[]);

// Pipes and Lists return most left cmd name
char* getCMDName (struct cmd* cmd);

// If no Internal, return 0
int isBuiltIn (const char* cmd);

bool isSpecialCMD (const char* cmd);

#endif /* __SPECIALCMD_H__ */
