#ifndef __CONST_H__
#define __CONST_H__

// Tipos presentes en la estructura `cmd`, campo `type`.
#define EXEC  1
#define REDIR 2
#define PIPE  3
#define LIST  4
#define BACK  5

#define MAXARGS 15
#define MAX_PATH_LENGTH 256
#define MAX_PROMPT_LENGTH 256
#define TEE_COUNT 1024
#define TEE_MAX_LOG_LINE 256
#define TIME_BUFFER_SIZE 128

#endif /* __CONST_H__ */
