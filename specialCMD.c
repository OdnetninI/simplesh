#include "common.h"

char* cmds[_CMDS] = {
  "exit", "cd", "pwd", "tee", "du"
};

void (*cmdsTable[_CMDS]) (char* argv[]) = {
  &exec_exit, &exec_cd, &exec_pwd, &exec_tee, &exec_du
};

void exec_internal (unsigned int number, char* argv[]) {
  if (number > _CMDS || number == 0) {
    panic("Internal Command not Found");
    exit(1);
  }
  cmdsTable[number-1](argv);
}

char* getCMDName (struct cmd* cmd) {
  if(cmd == 0)
    exit(0);

  switch(cmd->type) {
    case EXEC: return ((struct execcmd*)cmd)->argv[0];
    case REDIR: return getCMDName(((struct redircmd*)cmd)->cmd);
    case PIPE: return getCMDName(((struct pipecmd*)cmd)->left);
    case LIST: return getCMDName(((struct listcmd*)cmd)->left);
    case BACK: return getCMDName(((struct backcmd*)cmd)->cmd);
  }
  return "";
}

int isBuiltIn (const char* cmd) {
  if (cmd == NULL) return 0;
  for (unsigned int i = 0; i < _CMDS; i++)
    if (strcmp(cmds[i], cmd) == 0) return i+1;
  return 0;
}

bool isSpecialCMD (const char* cmd) {
  switch (isBuiltIn (cmd)) {
    case _CMD_EXIT:
    case _CMD_CD:

      return true;
  }
  return false;
}
