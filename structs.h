#ifndef __STRUCTS_H__
#define __STRUCTS_H__

// Estructuras
// -----

// La estructura `cmd` se utiliza para almacenar la información que
// servirá al shell para guardar la información necesaria para
// ejecutar las diferentes tipos de órdenes (tuberías, redirecciones,
// etc.)
//
// El formato es el siguiente:
//
//     |----------+--------------+--------------|
//     | (1 byte) | ...          | ...          |
//     |----------+--------------+--------------|
//     | type     | otros campos | otros campos |
//     |----------+--------------+--------------|
//
// Nótese cómo las estructuras `cmd` comparten el primer campo `type`
// para identificar el tipo de la estructura, y luego se obtendrá un
// tipo derivado a través de *casting* forzado de tipo. Se obtiene así
// un polimorfismo básico en C.
struct cmd {
  int type;
};

// Ejecución de un comando con sus parámetros
struct execcmd {
  int type;
  char * argv[MAXARGS];
  char * eargv[MAXARGS];
};

// Ejecución de un comando de redirección
struct redircmd {
  int type;
  struct cmd *cmd;
  char *file;
  char *efile;
  int mode;
  int fd;
};

// Ejecución de un comando de tubería
struct pipecmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

// Lista de órdenes
struct listcmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

// Tarea en segundo plano (background) con `&`.
struct backcmd {
  int type;
  struct cmd *cmd;
};

// Construye una estructura `EXEC`.
struct cmd* execcmd(void);

// Construye una estructura de redirección.
struct cmd* redircmd(struct cmd *subcmd, char *file, char *efile, int mode, int fd);

// Construye una estructura de tubería (*pipe*).
struct cmd* pipecmd(struct cmd *left, struct cmd *right);

// Construye una estructura de lista de órdenes.
struct cmd* listcmd(struct cmd *left, struct cmd *right);

// Construye una estructura de ejecución que incluye una ejecución en
// segundo plano.
struct cmd* backcmd(struct cmd *subcmd);

// Termina en NUL todas las cadenas de `cmd`.
struct cmd* nulterminate(struct cmd *cmd);

#endif /* __STRUCTS_H__ */
