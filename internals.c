#include "common.h"

void exec_exit() {
  //fprintf(stdout, "[simplesh]\n");
  exit(EXIT_SUCCESS);
}

void exec_pwd() {
  // Get Actual Dir
  char directory[MAX_PATH_LENGTH];
  if (!getcwd(directory, MAX_PATH_LENGTH)) {
    perror("getcwd");
    exit(EXIT_FAILURE);
  }

  fprintf(stderr, "simplesh: pwd: ");
  fprintf(stdout, "%s\n", directory);
}

void exec_cd (char* argv[]) {
  int error = 0;
  if (argv[1] == 0) error = chdir (getenv("HOME"));
  else error = chdir (argv[1]);
  if (error == -1)
    perror("simplesh: cd");
}

const  char *get_current_time(const  char* format) {
  static  char  buf[TIME_BUFFER_SIZE ];
  time_t t;
  struct  tm *tm;
  size_t s;
  t = time(NULL);
  tm = localtime (&t);
  if (tm == NULL)
  return  NULL;
  s = strftime(buf , TIME_BUFFER_SIZE , (format  != NULL) ? format : " %c (%Z)", tm);
  return (s == 0) ? NULL : buf;
}

void exec_tee (char* argv[]) {
  int argc = 0;
  while(argv[argc] != 0) argc++;

  bool flag = false;
  int c = 0;
  if (argc > 1) {
    while ((c = getopt(argc, argv, "ah")) != -1) {
      switch(c) {
        case 'h':
          fprintf(stdout, "Uso: tee [-h] [-a] [FICHERO ]...\nCopia  stdin a cada  FICHERO y a stdout.\nOpciones:\n    -a Añade al final de cada  FICHERO\n    -h help\n");
          exit(EXIT_SUCCESS);
        case 'a': flag = true;
      }
    }
  }
  int goodFiles = 1;
  int files[argc - optind + 1];
  files[0] = STDOUT_FILENO;
  for (int i = optind; i < argc; i++) {
    int od = 0;
    if (!flag) od = open(argv[i], O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);
    else od = open(argv[i], O_RDWR|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);
    if (od >= 0) {
      files[goodFiles] = od;
      goodFiles++;
    }
  }

  int count = 0;
  long long totalBytes = 0;
  char buf[TEE_COUNT];
  while ((count = read(STDIN_FILENO, buf, TEE_COUNT)) != 0) {
    //fprintf(stdout, "CABRON: %d\n", count);
    if (count == -1) {
      perror("read");
      exit(EXIT_FAILURE);
    }
    totalBytes += count;
    for (int i = 0; i < goodFiles; i++) {
      int wrote = 0;
      while (wrote < count) {
        wrote += write(files[i], buf+wrote, count - wrote);
        if (wrote < 0) { perror("write "); break;}
      }
    }
  }
  for (int i = 1; i < goodFiles; i++) {
    if (fsync(files[i]) == -1)
      perror("fsync");
    close(files[i]);
  }

  char logFilename[MAX_PATH_LENGTH];
  sprintf(logFilename, "%s/.tee.log", getenv("HOME"));
  int logFile = open(logFilename, O_RDWR|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);
  if (logFile < 0) {
    perror("open");
    exit(EXIT_FAILURE);
  }
  pid_t pid = getpid();
  uid_t euid = geteuid();
  char line[TEE_MAX_LOG_LINE];
  sprintf(line, "%s : PID %d : EUID %d : %lld byte(s) : %d file(s)\n", get_current_time("%F %T"), pid, euid, totalBytes, goodFiles-1);
  int wrote = 0;
  int toWrite = strlen(line);
  while (wrote < toWrite) {
    wrote += write(logFile, line+wrote, toWrite - wrote);
    if (wrote < 0) { perror("write "); break;}
  }
  if (fsync(logFile) == -1)
    perror("fsync");
  close(logFile);
}

static unsigned long long _du_sizeFiles = 0;
static bool du_bflag = false;
static bool du_vflag = false;
static bool du_loflag = true;
static unsigned long long int du_sizeLimit = 0;

static int display_info(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf) {
  if (S_ISREG(sb->st_mode)) {
    if (du_loflag) {
      if (du_sizeLimit >= sb->st_size) return 0;
    }
    else if (du_sizeLimit <= sb->st_size) return 0;

    if(!du_bflag) _du_sizeFiles += sb->st_size;
    else _du_sizeFiles += sb->st_blocks * 512;

    if (du_vflag) {
      unsigned long long int i = ftwbuf->base;
      for (long long int j = 0; j != ftwbuf->level+1 && i >= 0; i--)
        if (fpath[i] == '/') j++;
      for (unsigned long long int i = 0; i < ftwbuf->level; i++)
          fprintf(stdout, "    ");
      fprintf(stdout, "%-40s : %ld\n", fpath+i+2, (du_bflag)? sb->st_blocks * 512 : sb->st_size);
    }
  }
  else if (S_ISDIR(sb->st_mode)) {
    if (du_vflag) {
      for (unsigned long long int i = 0; i < ftwbuf->level; i++)
        fprintf(stdout, "    ");

      unsigned long long int i = ftwbuf->base;
      for (long long int j = 0; j != ftwbuf->level+1 && i >= 0; i--)
        if (fpath[i] == '/') j++;

      fprintf(stdout, "%s\n", fpath+i+2);
    }
  }
  /*
  printf("%-3s %2d %7jd   %-40s %d %s\n",
     (tflag == FTW_D) ?   "d"   : (tflag == FTW_DNR) ? "dnr" :
     (tflag == FTW_DP) ?  "dp"  : (tflag == FTW_F) ?   "f" :
     (tflag == FTW_NS) ?  "ns"  : (tflag == FTW_SL) ?  "sl" :
     (tflag == FTW_SLN) ? "sln" : "???",
     ftwbuf->level, (intmax_t) sb->st_size,
     fpath, ftwbuf->base, fpath + ftwbuf->base);
  */
  return 0;
}

void exec_du_under (const char* realpath, const char* displaypath, bool file) {
  if(nftw(realpath, display_info, 50, 0) == -1) {
    perror("nftw");
    exit(EXIT_FAILURE);
  }
  fprintf(stdout, "(%s) %s: %lld\n", (file)? "F" : "D", displaypath, _du_sizeFiles);
}

void exec_du (char* argv[]) {
  _du_sizeFiles = 0;
  du_bflag = false;
  du_vflag = false;
  du_loflag = true;

  char realPath[MAX_PATH_LENGTH];
  char directory[MAX_PATH_LENGTH];
  if (!getcwd(directory, MAX_PATH_LENGTH)) {
    perror("getcwd");
    exit(EXIT_FAILURE);
  }

  int argc = 0;
  while(argv[argc] != 0) argc++;

  int c = 0;
  if (argc > 1) {
    while ((c = getopt(argc, argv, "bvht:")) != -1) {
      switch(c) {
        case 'h':
          fprintf(stdout, "Uso: du [-h] [-b] [-t SIZE] [-v] [FICHERO|DIRECTORIO]...\nPara  cada  fichero , imprime  su tamaño.\nPara  cada  directorio , imprime  la suma de los  tamaños de todos  los  ficheros  de todos  sus  subdirectorios.\nOpciones:\n    -b Imprime  el tamaño ocupado  en  disco  por  todos  los bloques  del  fichero.\n    -t SIZE  Excluye  todos  los  ficheros más grandes que  SIZE  bytes , si es positivo , o más pequeños  que  SIZE  bytes , si es negativo , cuando  se procesa  un  directorio.\n    -v Imprime  el tamaño de  todos y cada  uno de los  ficheros  cuando  se  procesa  un directorio.\n    -h help\nNota: Todos  los  tamaños están expresados  en  byte\n");
          exit(EXIT_SUCCESS);
        case 'b': du_bflag = true; break;
        case 'v': du_vflag = true; break;
        case 't':
          if (optarg[0] == '-') { du_loflag = true; optarg[0] = '0';}
          else du_loflag = false;
          du_sizeLimit = atoll(optarg);
          break;
      }
    }
    if (optind == argc)
      exec_du_under(directory, ".", false);
    for(;optind < argc; optind++) {
      struct stat sb;
      _du_sizeFiles = 0;
      if (stat(argv[optind], &sb) == -1 ) continue;
      if (argv[optind][0] != '/')
        snprintf(realPath, MAX_PATH_LENGTH,"%s/%s", directory, argv[optind]);
      if (S_ISREG(sb.st_mode)) {
        exec_du_under(realPath, argv[optind], true);
      }
      else if (S_ISDIR(sb.st_mode)){
        exec_du_under(realPath, argv[optind], false);
      }
    }
  }
  else
    exec_du_under(directory, ".", false);
}
