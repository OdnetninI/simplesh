#ifndef __RUNCMD_H__
#define __RUNCMD_H__

// Ejecuta un `cmd`.
void run_cmd(struct cmd *cmd);

#endif /* __RUNCMD_H__ */
