#ifndef __SIGNAL_HANDLER_H__
#define __SIGNAL_HANDLER_H__

#include "common.h"

void createSignals ();

void waitAndKillChild(pid_t pid);

#endif /* __SIGNAL_HANDLER_H__ */
